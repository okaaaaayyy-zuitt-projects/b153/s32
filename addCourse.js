// Get the #createCourse element for the HTML file via querySelector

	let formSubmit = document.querySelector("#createCourse")

	formSubmit.addEventListener("submit", (e) => {
	// Make sure that the form's default behavior is prevented
	e.preventDefault()

// Get the input values for #courseName, #courseDescription, and #coursePrice


	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

// Use POST request to send the input's values to the /courses endpoint

	fetch("http://localhost:4000/courses", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}` // Make sure that you include the proper authorization header and its value
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {
		if(data === true){
		// If successful, redirect the user to /courses.html.
		window.location.replace("./courses.html")	
		}else{
		// If not, show an error alert
		alert("Course not added. Please try again.")
		}
	})
})


// Check to make sure courses are successfully added

// When done, create an a5 folder in activities and push it to Gitlab, then paste the link to Boodle.